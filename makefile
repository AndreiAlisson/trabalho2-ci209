CFLAGS = -g -Wall -Wno-unused-result -O3
LDLIBS = -l hiredis -l readline

all: raposa

controlador: controlador.c

raposa: tabuleiro.h tabuleiro.c raposa.c

clean:
	rm -f raposa