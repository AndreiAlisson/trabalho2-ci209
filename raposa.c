#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "tabuleiro.h"

#define MAXSTR 512
#define MAXINT 16
#define BOARD_SIZE 7
#define REAL_BOARD_SIZE (BOARD_SIZE + 3)

#define BUFFER_LENGTH 2048
#define BUFFER_DEPTH 8

#define XAXIS(i) ((i) % REAL_BOARD_SIZE)
#define YAXIS(i) ((i) * Inverse_Board_Size)
#define INDEX(x,y) ((y) * REAL_BOARD_SIZE + (x))

#define INV -100000		// INVALID
#define HOR 1					// HORRIBLE
#define BAD 4					// BAD
#define NOR 8					// NORMAL
#define GOO 16				// GOOD

#define STR_PENALTY 1		// Strong 
#define NOR_PENALTY	1	 	// Normal
#define WEA_PENALTY	1		// Weak

#define MAXDEPTH 7

#define INF 999999999

const float Inverse_Board_Size = 1.0 / REAL_BOARD_SIZE;

const int positions_score[] = {
	INV, INV, INV,   INV, INV, INV,   INV, INV, INV, INV,
	INV, INV, INV,   HOR, HOR, HOR,   INV, INV, INV, INV,
	INV, INV, INV,   BAD, BAD, BAD,   INV, INV, INV, INV,
	INV,   HOR, BAD, NOR, NOR, NOR, BAD, HOR,   INV, INV,
	INV,   HOR, BAD, NOR, GOO, NOR, BAD, HOR,   INV, INV,
	INV,   HOR, BAD, NOR, NOR, NOR, BAD, HOR,   INV, INV,
	INV, INV, INV,   BAD, BAD, BAD,   INV, INV, INV, INV,
	INV, INV, INV,   HOR, HOR, HOR,   INV, INV, INV, INV,
	INV, INV, INV,   INV, INV, INV,   INV, INV, INV, INV
};

typedef struct State {
	int g[14];
	int g_size;
	int r;
} State;

typedef struct Comand {
	char who;
	char type;
	int x, y;
	int size;
	int xtarget[16], ytarget[16];
} Comand;

typedef struct GMoves {
	int size;
	int movs[4];
} GMoves;

State* string_to_state(char *string);
int minimax(State *state, int depth, int alfa, int beta, bool maximize);

static Comand comands[4];
static Comand *chosen;

static int gsx[] = {2,3,4,5,6,3,4,5,4};
static int gsy[] = {3,3,3,3,3,2,2,2,1};

unsigned int hash(State *state) {
	unsigned int h = 5381;

	for (int i = 0; i < state->g_size; i++) 
		h = ((h << 5) + h) ^ state->g[i];
	h = ((h << 5) + h) ^ state->r;

	return h % BUFFER_LENGTH;
}

void print_comand(Comand *comand) {
	printf("%c %c %d %d ", comand->who, comand->type, comand->y, comand->x);
	for (int i = 0; i < comand->size; i++)
		printf("%d %d ", comand->ytarget[i], comand->xtarget[i]);
	printf("\n");
}

void comand_to_string(Comand *comand, char *buffer) {
	int a = 0;

	a += sprintf(buffer, "%c %c ", comand->who, comand->type);

	if (comand->type == 's')
		a += sprintf(buffer + a, "%d ", comand->size + 1);

	a += sprintf(buffer + a, "%d %d ", comand->y, comand->x);

	for (int i = 0; i < comand->size; i++)
		a += sprintf(buffer + a, "%d %d ", comand->ytarget[i], comand->xtarget[i]);
	sprintf(buffer + a, "\n");
}

int main(int argc, char **argv) {
  char buf[MAXSTR];
  char tabuleiro[MAXSTR]; 
  char lado_meu;
  char lado_adv;
  char tipo_mov_adv;
  int num_mov_adv;
  int mov_adv_l[MAXINT];
  int mov_adv_c[MAXINT];
  int i;
	static GMoves gs[9];

  // conecta com o controlador do campo
  tabuleiro_conecta(argc, argv);

	srand(time(NULL));

	do {
  	// recebe o campo inicial e o movimento do adversario
  	tabuleiro_recebe(buf);

  	// separa os elementos do string recebido
  	sscanf(strtok(buf, " \n"), "%c", &lado_meu);
  	sscanf(strtok(NULL, " \n"), "%c", &lado_adv);
  	sscanf(strtok(NULL, " \n"), "%c", &tipo_mov_adv);
  	if(tipo_mov_adv == 'm') {
  	  num_mov_adv = 2;
  	  for(i = 0; i < num_mov_adv; i++) {
  	    sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_l[i]));
  	    sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_c[i]));
  	  }
  	}
  	else if(tipo_mov_adv == 's') {
  	  sscanf(strtok(NULL, " \n"), "%d", &num_mov_adv);
  	  for(i = 0; i < num_mov_adv; i++) {
  	    sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_l[i]));
  	    sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_c[i]));
  	  }
  	}
  	strncpy(tabuleiro, strtok(NULL, "."), MAXSTR);

		printf("%s", tabuleiro);
		
		if (lado_meu == 'r') {
			minimax(string_to_state(&tabuleiro[0]), MAXDEPTH, -INF, INF, true);
			comand_to_string(chosen, buf);
		}
		else {
			for (int j = 0; j < 9; j++) {
				if (tabuleiro[INDEX(gsx[j], gsy[j])] == 'g') {
					int gindex = INDEX(gsx[j]-1, gsy[j]);
		
					gs[j].size = 0;

					if (tabuleiro[gindex] == '-')
						gs[j].movs[(gs[j].size)++] = gindex;
					gindex = INDEX(gsx[j]+1, gsy[j]);
					if (tabuleiro[gindex] == '-')
						gs[j].movs[(gs[j].size)++] = gindex;
					gindex = INDEX(gsx[j], gsy[j]-1);
					if (tabuleiro[gindex] == '-')
						gs[j].movs[(gs[j].size)++] = gindex;
					gindex = INDEX(gsx[j], gsy[j]+1);
					if (tabuleiro[gindex] == '-')
						gs[j].movs[(gs[j].size)++] = gindex;
				}
			}

			for (;;) {
				int rg = rand() % 9;

				if (tabuleiro[INDEX(gsx[rg], gsy[rg])] != 'g' || gs[rg].size == 0)
					continue;
				
				int rm = rand() % gs[rg].size;
				int x = (int)XAXIS(gs[rg].movs[rm]);
				int y = (int)YAXIS(gs[rg].movs[rm]);

				sprintf(buf, "g m %d %d %d %d\n", gsy[rg], gsx[rg], y, x);
				gsx[rg] = x, gsy[rg] = y;

				break;
			}
		}

  	// envia o movimento para o controlador do campo
  	tabuleiro_envia(buf);  
	} while (1);
}

State* string_to_state(char *string) {
	State *state = calloc(1, sizeof(State));

	for (int i = 0; i < MAXSTR; i++)
		switch (string[i]) {
			case 'g':
				state->g[(state->g_size)++] = i;
				break;
			case 'r':
				state->r = i;
				break;
		}

	return state;
}

bool has_g(State *state, int index) {
	for (int i = 0; i < state->g_size; i++)
		if (state->g[i] == index)
			return true;
	return false;
}

int heuristic(State *state) {
	int penalty = 0;

	for (int i = 0; i < state->g_size; i++) {
		int gx = XAXIS(state->g[i]);
		int gy = YAXIS(state->g[i]);

		int a, b;
		bool xaxis, yaxis;

		a = INDEX(gx-1,gy);
		b = INDEX(gx+1,gy);
		xaxis =	gx-1 < 0 || positions_score[a] == INV || has_g(state, a) ||
		 				gx+1 == BOARD_SIZE || positions_score[b] == INV || has_g(state, b);

		a = INDEX(gx,gy-1);
		b = INDEX(gx,gy+1);
		yaxis = gy-1 < 0 || positions_score[a] == INV || has_g(state, a) ||
						gy+1 == BOARD_SIZE || positions_score[b] == INV || has_g(state, b);

		if (xaxis && yaxis)
			penalty += STR_PENALTY;
		else if (xaxis ^ yaxis)
			penalty += NOR_PENALTY;
		else
			penalty += WEA_PENALTY;
	}

	return positions_score[state->r] - penalty;
}

bool compute_jump(State *state, int jx, int jy, State *buffer, int cmd, 
									int depth, bool store_comand) {
	if (has_g(state, INDEX(jx, jy)))
		return false;

	int x = XAXIS(state->r);
	int y = YAXIS(state->r);
	
	int dead = INDEX(x+((jx-x)>>1), y+((jy-y)>>1));

	for (int i = 0, j = 0; i < state->g_size; i++)
		if (state->g[i] != dead)
			buffer->g[j++] = state->g[i];
	buffer->g_size = state->g_size-1;
	buffer->r = INDEX(jx, jy);

	if (store_comand) {
		if (depth == 0) {
			comands[cmd].who = 'r';
			comands[cmd].type = 's';
			comands[cmd].x = x, comands[cmd].y = y;
			comands[cmd].size = 1;
			comands[cmd].xtarget[0] = jx, comands[cmd].ytarget[0] = jy;
		}
		else {
			(comands[cmd].size)++;
			comands[cmd].xtarget[depth] = jx, comands[cmd].ytarget[depth] = jy;
		}
	}

	if (positions_score[INDEX(jx-1,jy)] != INV && has_g(buffer, INDEX(jx-1,jy)) && 
			positions_score[INDEX(jx-2,jy)] != INV)
			compute_jump(buffer, jx-2, jy, buffer, cmd, depth+1, store_comand);
	else if (	positions_score[INDEX(jx+1,jy)] != INV && 
						has_g(buffer, INDEX(jx+1,jy)) && 
						positions_score[INDEX(jx+2,jy)] != INV)
			compute_jump(buffer, jx+2, jy, buffer, cmd, depth+1, store_comand);
	else if (	positions_score[INDEX(jx,jy-1)] != INV && 
						has_g(buffer, INDEX(jx,jy-1)) && 
						positions_score[INDEX(jx,jy-2)] != INV)
			compute_jump(buffer, jx, jy-2, buffer, cmd, depth+1, store_comand);
	else if (	positions_score[INDEX(jx,jy+1)] != INV && 
						has_g(buffer, INDEX(jx,jy+1)) && 
						positions_score[INDEX(jx,jy+2)] != INV)
			compute_jump(buffer, jx, jy+2, buffer, cmd, depth+1, store_comand);

	return true;
}

bool compute_rmove(	State *state, int mx, int my, State *buffer, int cmd, 
										bool store_comand) {
	if (has_g(state, INDEX(mx, my)))
		return false;

	for (int i = 0; i < state->g_size; i++)
		buffer->g[i] = state->g[i];
	buffer->g_size = state->g_size;

	buffer->r = INDEX(mx, my);

	if (store_comand) {
		comands[cmd].who = 'r';
		comands[cmd].type = 'm';
		comands[cmd].x = XAXIS(state->r), comands[cmd].y = YAXIS(state->r);
		comands[cmd].size = 1;
		comands[cmd].xtarget[0] = mx, comands[cmd].ytarget[0] = my;
	}

	return true;
}

bool compute_gmove(State *state, int mx, int my, int gi, State *buffer) {
	if (has_g(state, INDEX(mx, my)))
		return false;

	for (int i = 0; i < state->g_size; i++)
		buffer->g[i] = state->g[i] == gi? INDEX(mx,my) : state->g[i];
	buffer->g_size = state->g_size;

	buffer->r = state->r;

	return true;
}

State* rbranch(State *state, int *size, bool store_comand) {
	State *new_states = calloc(4, sizeof(State));
	int x = XAXIS(state->r);
	int y = YAXIS(state->r);

	*size = 0;

	if (positions_score[INDEX(x-1,y)] != INV) {
		if (compute_rmove(state, x-1, y, &new_states[*size], *size, store_comand))
			(*size)++;
		else if (	positions_score[INDEX(x-2,y)] != INV && 
							compute_jump(state, x-2, y, &new_states[*size], *size, 0, store_comand))
			(*size)++;
	}
	if (positions_score[INDEX(x+1,y)] != INV) {
		if (compute_rmove(state, x+1, y, &new_states[*size], *size, store_comand))
			(*size)++;
		else if (	positions_score[INDEX(x+2,y)] != INV && 
							compute_jump(state, x+2, y, &new_states[*size], *size, 0, store_comand))
			(*size)++;
	}
	if (positions_score[INDEX(x,y-1)] != INV) {
		if (compute_rmove(state, x, y-1, &new_states[*size], *size, store_comand))
			(*size)++;
		else if ( positions_score[INDEX(x,y-2)] != INV && 
							compute_jump(state, x, y-2, &new_states[*size], *size, 0, store_comand))
			(*size)++;
	}
	if (positions_score[INDEX(x,y+1)] != INV) {
		if (compute_rmove(state, x, y+1, &new_states[*size], *size, store_comand))
			(*size)++;
		else if (	positions_score[INDEX(x,y+2)] != INV &&
							compute_jump(state, x, y+2, &new_states[*size], *size, 0, store_comand))
			(*size)++;
	}		

	return new_states;
}

State* gbranch(State *state, int *size) {
	State *new_states = calloc(state->g_size*4, sizeof(State));

	*size = 0;

	for (int i = 0; i < state->g_size; i++) {
		int x = XAXIS(state->g[i]);
		int y = YAXIS(state->g[i]);

		if (positions_score[INDEX(x-1,y)] != INV)
			if (compute_gmove(state, x-1, y, state->g[i], &new_states[*size]))
				(*size)++;
		if (positions_score[INDEX(x+1,y)] != INV)
			if (compute_gmove(state, x+1, y, state->g[i], &new_states[*size]))
				(*size)++;
		if (positions_score[INDEX(x,y-1)] != INV)
			if (compute_gmove(state, x, y-1, state->g[i], &new_states[*size]))
				(*size)++;
		if (positions_score[INDEX(x,y+1)] != INV)
			if (compute_gmove(state, x, y+1, state->g[i], &new_states[*size]))
				(*size)++;
	}

	return new_states;
}

int minimax(State *state, int depth, int alfa, int beta, bool maximize) {
	State *childs;
	int size, score;

	if (depth == 0)
		return heuristic(state);

	if (maximize) {
		score = -INF;
		childs = rbranch(state, &size, depth == MAXDEPTH);

		/* O jogo acabou, a raposa perdeu */
		if (size == 0)
			return heuristic(state);

		for (int i = 0; i < size; i++) {
			int ret = minimax(&(childs[i]), depth-1, alfa, beta, false);

			if (score < ret) {
				score = ret;
				chosen = &(comands[i]);
			}
			if (alfa > score)
				alfa = score;

			if (alfa >= beta)
				break;
		}
	}
	else {
		score = INF;
		childs = gbranch(state, &size);

		/* O jogo acabou, a raposa ganhou */
		if (size == 0)
			return heuristic(state);

		for (int i = 0; i < size; i++) {
			int ret = minimax((&childs[i]), depth-1, alfa, beta, true);

			if (score > ret)
				score = ret;
			if (beta < score)
				beta = score;

			if (alfa >= beta)
				break; 
		}
	}

	free(childs);

	return score;
}
