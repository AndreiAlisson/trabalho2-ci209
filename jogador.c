#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "tabuleiro.h"

#define MAXSTR 512
#define MAXINT 16
#define MAXS 50

#define POS(l,c) ((l) * 10 + (c))

enum Players {Raposa = 'r', Ganso = 'g'};

char buf[MAXSTR];

/*Jogador raposa analisando a jogada feita pelo ganso (Versão gulosa/random por enquanto)*/
void Analisa_Mov_Ganso(char *tabuleiro, int *r_pos_l, int *r_pos_c){

  int pos_l = *r_pos_l;
  int pos_c = *r_pos_c;

  //printf("posição r init: %d, %d\n", pos_l, pos_c);

  if(tabuleiro[POS((pos_l + 1), pos_c)] == Ganso && tabuleiro[POS((pos_l + 2), pos_c)] != Ganso && (pos_l + 2) <= 7 && ((pos_l + 2) <= 5  || (pos_c >= 3 && pos_c <= 5))){  
      sprintf(buf, "r s 2 %d %d %d %d\n", pos_l, pos_c, (pos_l + 2), pos_c);
      *r_pos_l = pos_l + 2;
      *r_pos_c = pos_c;
  }
  else if(tabuleiro[POS(pos_l, (pos_c + 1))] == Ganso && tabuleiro[POS(pos_l, (pos_c + 2))] != Ganso && (pos_c + 2) <= 7 && ((pos_c + 2) <= 5  || (pos_l >= 3 && pos_l <= 5))){      
      sprintf(buf, "r s 2 %d %d %d %d\n", pos_l, pos_c, pos_l, (pos_c + 2));
      *r_pos_l = pos_l;
      *r_pos_c = pos_c + 2;
  }
  else if(tabuleiro[POS((pos_l - 1), pos_c)] == Ganso && tabuleiro[POS((pos_l - 2), pos_c)] != Ganso && (pos_l - 2) >= 1 && ((pos_l - 2) >= 3  || (pos_c >= 3 && pos_c <= 5))){
      sprintf(buf, "r s 2 %d %d %d %d\n", pos_l, pos_c, (pos_l - 2), pos_c);
      *r_pos_l = pos_l - 2;
      *r_pos_c = pos_c;
  }
  else if(tabuleiro[POS(pos_l, (pos_c - 1))] == Ganso && tabuleiro[POS(pos_l, (pos_c - 2))] != Ganso && (pos_c - 2) >= 1 && ((pos_c - 2) >= 3  || (pos_l >= 3 && pos_l <= 5))){      
      sprintf(buf, "r s 2 %d %d %d %d\n", pos_l, pos_c, pos_l, (pos_c - 2));
      *r_pos_l = pos_l;
      *r_pos_c = pos_c - 2;
  }
  else{
    int cond = 0;

    do{
      int rd = rand() % 4;
 
      if(rd == 0){
        if((pos_l + 1) <= 7 && ((pos_l + 1) <= 5  || (pos_c >= 3 && pos_c <= 5)) && tabuleiro[POS((pos_l + 1), pos_c)] != Ganso){
            cond = 1;
            sprintf(buf, "r m %d %d %d %d\n", pos_l, pos_c, (pos_l + 1), pos_c);
            *r_pos_l = pos_l + 1;
            *r_pos_c = pos_c;
        }
      }
      else if(rd == 1){
        if((pos_l - 1) >= 1 && ((pos_l - 1) >= 3  || (pos_c >= 3 && pos_c <= 5)) && tabuleiro[POS((pos_l - 1), pos_c)] != Ganso){
            cond = 1;
            sprintf(buf, "r m %d %d %d %d\n", pos_l, pos_c, (pos_l - 1), pos_c);
            *r_pos_l = pos_l - 1;
            *r_pos_c = pos_c;
        }
      }
      else if(rd == 2){
        if((pos_c + 1) <= 7 && ((pos_c + 1) <= 5  || (pos_l >= 3 && pos_l <= 5)) && tabuleiro[POS(pos_l, (pos_c + 1))] != Ganso){
            cond = 1;
            sprintf(buf, "r m %d %d %d %d\n", pos_l, pos_c, pos_l, (pos_c + 1));
            *r_pos_l = pos_l;
            *r_pos_c = pos_c + 1;
        }
      }
      else{
        if((pos_c - 1) >= 1 && ((pos_c - 1) >= 3  || (pos_l >= 3 && pos_l <= 5)) && tabuleiro[POS(pos_l, (pos_c - 1))] != Ganso){
            cond = 1;
            sprintf(buf, "r m %d %d %d %d\n", pos_l, pos_c, pos_l, (pos_c - 1));
            *r_pos_l = pos_l;
            *r_pos_c = pos_c - 1;
        }
      }

    }while(!cond);

  }

  printf("posição r final: %d, %d\n", *r_pos_l, *r_pos_c);

}

/*Jogador ganso analisando a jogada feita pela raposa(Versão gulosa/random por enquanto)*/
void Analisa_Mov_Raposa(char *tabuleiro, int r_pos_l, int r_pos_c){

  int pos_l, pos_c;
  int cond;

  do{

    int rd = random() % 49;
    cond = 0;

    pos_l = rd / 7;
    pos_c = rd % 7;

    if(tabuleiro[POS(pos_l, pos_c)] == Ganso){
      if(tabuleiro[POS((pos_l + 1), pos_c)] != Ganso && tabuleiro[POS((pos_l + 1), pos_c)] != Raposa){
        if((pos_l + 1) <= 7 && ((pos_l + 1) <= 5  || (pos_c >= 3 && pos_c <= 5))){      
          sprintf(buf, "g m %d %d %d %d\n", pos_l, pos_c, (pos_l + 1), pos_c);
          cond = 1;
        }
      }
      else if(tabuleiro[POS(pos_l, (pos_c + 1))] != Ganso && tabuleiro[POS(pos_l, (pos_c + 1))] != Raposa){
        if((pos_c + 1) <= 7 && ((pos_c + 1) <= 5  || (pos_l >= 3 && pos_l <= 5))){      
          sprintf(buf, "g m %d %d %d %d\n", pos_l, pos_c, pos_l, (pos_c + 1));
          cond = 1;
        }
      }
      else if(tabuleiro[POS((pos_l - 1), pos_c)] != Ganso && tabuleiro[POS((pos_l - 1), pos_c)] != Raposa){
        if((pos_l - 1) >= 1 && ((pos_l - 1) >= 3  || (pos_c >= 3 && pos_c <= 5))){     
          sprintf(buf, "g m %d %d %d %d\n", pos_l, pos_c, (pos_l - 1), pos_c);
          cond = 1;
        }
      }
      else if(tabuleiro[POS(pos_l, (pos_c - 1))] != Ganso && tabuleiro[POS(pos_l, (pos_c - 1))] != Raposa){
        if((pos_c - 1) >= 1 && ((pos_c - 1) >= 3  || (pos_l >= 3 && pos_l <= 5))){       
          sprintf(buf, "g m %d %d %d %d\n", pos_l, pos_c, pos_l, (pos_c - 1));
          cond = 1;
        }
      }
    }


  }while(!cond);

}

int main(int argc, char **argv) {
  char tabuleiro[MAXSTR]; 
  char lado_meu;
  char lado_adv;
  char tipo_mov_adv;
  int num_mov_adv;
  int mov_adv_l[MAXINT];
  int mov_adv_c[MAXINT];
  int i, j;
  int r_pos_l, r_pos_c;
  char *buffer;
  srand(time(NULL));
  
  tabuleiro_conecta(argc, argv);

  r_pos_l = 5;
  r_pos_c = 4;

  while(1) {

  // recebe o campo inicial e o movimento do adversario
  tabuleiro_recebe(buf);

  // separa os elementos do string recebido
  sscanf(strtok(buf, " \n"), "%c", &lado_meu);
  sscanf(strtok(NULL, " \n"), "%c", &lado_adv);
  sscanf(strtok(NULL, " \n"), "%c", &tipo_mov_adv);
  if(tipo_mov_adv == 'm') {
    num_mov_adv = 2;
    for(i = 0; i < num_mov_adv; i++) {
      sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_l[i]));
      sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_c[i]));
    }
  }
  else if(tipo_mov_adv == 's') {
    sscanf(strtok(NULL, " \n"), "%d", &num_mov_adv);
    for(i = 0; i < num_mov_adv; i++) {
      sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_l[i]));
      sscanf(strtok(NULL, " \n"), "%d", &(mov_adv_c[i]));
    }
  }
  strncpy(tabuleiro, strtok(NULL, "."), MAXSTR);

  // mostra o que recebeu
  printf("%c\n", lado_meu);
  printf("%c %c", lado_adv, tipo_mov_adv);
  if(tipo_mov_adv != 'n') {
    if(tipo_mov_adv == 's') 
      printf(" %d", num_mov_adv);
    for(i = 0; i < num_mov_adv; i++) {
      printf(" %d", mov_adv_l[i]);
      printf(" %d", mov_adv_c[i]);
    }
  }
  printf("\n");
  printf("%s", tabuleiro);
  
  // prepara um string com o movimento
  if(lado_meu == Raposa){
    Analisa_Mov_Ganso(tabuleiro, &r_pos_l, &r_pos_c);    
  }
  else if(lado_meu == Ganso){
    Analisa_Mov_Raposa(tabuleiro, r_pos_l, r_pos_c);
  }

  // envia o movimento para o controlador do campo
  tabuleiro_envia(buf);  


  }
}
